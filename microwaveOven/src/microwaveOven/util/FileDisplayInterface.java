package microwaveOven.util;

import java.io.File;

public interface FileDisplayInterface {
	
	void writeToFile(File f, String s);

}
