package microwaveOven.util;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Results implements FileDisplayInterface, StdoutDisplayInterface{
	public static PrintWriter writer;
	
	//constructor to initialize output file
	public Results(String f) {
		 try {
			writer = new PrintWriter(f, "UTF-8");
		} catch (FileNotFoundException e) {
			System.err.println("The output file was not found. Please provide an output file");
			e.printStackTrace();
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.err.println("The encoding UTF 8 is not dupported with this output file");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
 

//implementation of interface methods	
	public void writeToFile(File f, String s){
		try {
			
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "utf-8")); 
			writer.write(s);
		    
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void writeToStdout( String s){
		System.out.println("The string is"+s+"\n");
	}

//function to store results in output file.	
	public void storeNewResult(String s1){
		
		writer.println(s1);
		
	}
	
	
}
