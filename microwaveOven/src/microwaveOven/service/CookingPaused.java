package microwaveOven.service;

import microwaveOven.util.Results;
//class representing the paused state of cooking
public class CookingPaused implements MicrowaveStateI {
	
public MicrowaveStateI action(char keycode, Results r1){
		
		
		MicrowaveStateI idle = new IdleTime();
		MicrowaveStateI cooking = new Cooking();
		MicrowaveStateI cookPaused = new CookingPaused();
		
		
		
		MicrowaveStateI tempRes = new Cooking();
		
		
		
		switch(keycode) {
				
			case 'a': //resume cooking
				r1.storeNewResult("CookingPaused: cooking resumed for" + DispNo.timeSec+ "seconds");
				
				tempRes = cooking;
				break;
				
				//cancel cooking
			case 'b': r1.storeNewResult("CookingPaused: Cooking has been cancelled");
				DispNo.timeSec="0";
				tempRes = idle;
				break;
			case 'c': r1.storeNewResult("CookingPaused:clockset button is ignored");	
			    tempRes = cookPaused;
				break;
			default: tempRes = cookPaused;	
		}
		return tempRes;
	}

}
