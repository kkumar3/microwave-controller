package microwaveOven.service;

import microwaveOven.util.Results;

public class ClockReceiveInput implements MicrowaveStateI{
	
	//class representing the state when the machine is receiving input for clockset mechanism.
	
		public static String timeInput = "1120"; //static time variable to maintain uniformity.
		MicrowaveStateI temp;
		
		public MicrowaveStateI action(char keycode, Results r1){
			
			
			
			//MicrowaveStateI dispNo = new DispNo();
			MicrowaveStateI idle = new IdleTime();
			//MicrowaveStateI cooking = new Cooking();
			
			MicrowaveStateI clockSet = new ClockReceiveInput();
			
			MicrowaveStateI tempRes = new IdleTime();
			

			
			switch(keycode) {
				case '0': if(timeInput.length() < 4){
					r1.storeNewResult("0");
							timeInput = timeInput + "0";
							
						}
						else{
							r1.storeNewResult("Beep"); //if length>4 sound a beep
						}
				tempRes = clockSet;
						break;
				case '1': if(timeInput.length() < 4){
					r1.storeNewResult("1");
					timeInput = timeInput + "1";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '2':if(timeInput.length() < 4){
					r1.storeNewResult("2");
					timeInput = timeInput + "2";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '3':if(timeInput.length() < 4){
					r1.storeNewResult("3");
					timeInput = timeInput + "3";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '4':if(timeInput.length() < 4){
					r1.storeNewResult("4");
					timeInput = timeInput + "4";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '5':if(timeInput.length() < 4){
					r1.storeNewResult("5");
					timeInput = timeInput + "5";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '6':if(timeInput.length() < 4){
					r1.storeNewResult("6");
					timeInput = timeInput + "6";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '7':if(timeInput.length() < 4){
					r1.storeNewResult("7");
					timeInput = timeInput + "7";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '8':if(timeInput.length() < 4){
					r1.storeNewResult("8");
					timeInput = timeInput + "8";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
				case '9':if(timeInput.length() < 4){
					r1.storeNewResult("9");
					timeInput = timeInput + "9";
					
				}
				else{
					r1.storeNewResult("Beep");
				}
				tempRes = clockSet;
				break;
					
				case 'a':  // set the current clock time to timeInput variable
					
					if(timeInput.length()>2){
						r1.storeNewResult("ClockReceiveInput: Time is set to " + timeInput);
						tempRes=idle;
					}
					else{
						tempRes = clockSet;
					}
					break;
					
				default: tempRes = clockSet;	
			}
			return tempRes;
		}
		
	}	

