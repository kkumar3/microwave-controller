package microwaveOven.service;

import java.util.concurrent.TimeUnit;

import microwaveOven.util.Results;
//class representing the cooking state of machine

public class Cooking implements MicrowaveStateI{

	
	
public MicrowaveStateI action(char keycode , Results r1){
		
		//MicrowaveStateI dispNo = new DispNo();
		MicrowaveStateI idle = new IdleTime();
		MicrowaveStateI cooking = new Cooking();
		MicrowaveStateI cookPaused = new CookingPaused();
		
		//MicrowaveStateI clockSet = new ClockReceiveInput();
		
		MicrowaveStateI tempRes = new Cooking();
		long cStartTime = System.nanoTime(); //storing current time
		
		
		
		switch(keycode) {  // checking elapsed time before performing an action while food is being cooked
				
			case 'a': if(((System.nanoTime() - cStartTime)/1000000000.0) < Integer.valueOf(DispNo.timeSec)){
				r1.storeNewResult("Cooking: Im already cooking");
				
			}
			tempRes = cooking;
				break;
			case 'b': if(((System.nanoTime() - cStartTime)/1000000000.0) < Integer.valueOf(DispNo.timeSec)){
				r1.storeNewResult("Cooking: Cooking has been paused");
				tempRes = cookPaused;
				}
			else{ tempRes = idle;}
				break;
				
			case 'c': r1.storeNewResult("Cooking: clockset button is ignored");	
				break;
			
			case 'w' : 	
					r1.storeNewResult("Cooking: Waiting for cooking to be completed");
				try {
				TimeUnit.SECONDS.sleep(Integer.valueOf(DispNo.timeSec));
			} catch (NumberFormatException e) {
		
				e.printStackTrace();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			r1.storeNewResult("Cooking: Cooking completed, returning state to Idle");
			DispNo.timeSec = "0";
			tempRes=idle;	
			break;
			default: tempRes = cooking;	
		}
		
		return tempRes;
	}
}
