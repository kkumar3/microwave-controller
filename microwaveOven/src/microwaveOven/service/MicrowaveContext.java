package microwaveOven.service;

import microwaveOven.util.Results;

public class MicrowaveContext implements MicrowaveStateI{
	
	
	
	MicrowaveStateI currState;
	
	static MicrowaveStateI idle = new IdleTime();
	
	//Constructor to initialize the idle state
	public MicrowaveContext(){
		currState = idle;
	}
		
	//Action method invoking on the current state and passes the results variable.
	public MicrowaveStateI action(char keycode, Results r1) {
		
		
		currState = currState.action(keycode, r1);
		
		return currState;
	}
	

}
