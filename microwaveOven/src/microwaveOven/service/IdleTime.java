package microwaveOven.service;

import microwaveOven.util.Results;




public class IdleTime implements MicrowaveStateI{
	
	
	MicrowaveStateI temp;
	
	//class representing the Idle state of the machine which displays time
	public MicrowaveStateI action(char keycode, Results r1){
		
		
		
		MicrowaveStateI dispNo = new DispNo();
		MicrowaveStateI idle = new IdleTime();
		//MicrowaveStateI cooking = new Cooking();
		
		MicrowaveStateI clockSet = new ClockReceiveInput();
		
		MicrowaveStateI tempRes = new IdleTime();
		
		r1.storeNewResult("Idle: Time of day is " + ClockReceiveInput.timeInput);
		
		switch(keycode) {
			case '0': 	r1.storeNewResult("0");
						DispNo.timeSec = DispNo.timeSec + "0";
						tempRes = dispNo;
						break;
			case '1': r1.storeNewResult("1");
			DispNo.timeSec = DispNo.timeSec + "1";
					tempRes = dispNo;
					break;
			case '2': r1.storeNewResult("2");
			DispNo.timeSec = DispNo.timeSec + "2";
					tempRes = dispNo;
					break;
			case '3': r1.storeNewResult("3");
			DispNo.timeSec = DispNo.timeSec + "3";
					tempRes = dispNo;
					break;
			case '4':r1.storeNewResult("4");
			DispNo.timeSec = DispNo.timeSec + "4";
					tempRes = dispNo;
					break;
			case '5':r1.storeNewResult("5");
			DispNo.timeSec = DispNo.timeSec + "5";
					tempRes = dispNo;
					break;
			case '6':r1.storeNewResult("6");
			DispNo.timeSec = DispNo.timeSec + "6";
					tempRes = dispNo;
					break;
			case '7':r1.storeNewResult("7");
			DispNo.timeSec = DispNo.timeSec + "7";
					tempRes = dispNo;
					break;
			case '8':r1.storeNewResult("8");
			DispNo.timeSec = DispNo.timeSec + "8";
					tempRes = dispNo;
					break;
			case '9':r1.storeNewResult("9");
			DispNo.timeSec = DispNo.timeSec + "9";
					tempRes = dispNo;
					break;
				
			case 'a':
				r1.storeNewResult("Enter a time for cooking");
				
				break;
			case 'b': r1.storeNewResult("From Idle time state: Nothing to Cancel");
				break;
			case 'c': r1.storeNewResult("Entering clock editing mode");
					ClockReceiveInput.timeInput = ""; 		//reset the time to empty string
					tempRes = clockSet;	
				break;
			default: tempRes = idle;	
		}
		return tempRes;
	}
	

}