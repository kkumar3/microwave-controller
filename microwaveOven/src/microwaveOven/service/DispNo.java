package microwaveOven.service;

import microwaveOven.util.Results;

//class representing the State when the number buttons are fed into the machine.
public class DispNo implements MicrowaveStateI{

	public static String timeSec =""; //static time duration variable to maintain uniformity among other classes 
	MicrowaveStateI temp;
	
	//action method 
	public MicrowaveStateI action(char keycode, Results r1){
		
		MicrowaveStateI dispNo = new DispNo();
		MicrowaveStateI idle = new IdleTime();
		MicrowaveStateI cooking = new Cooking();
		
		MicrowaveStateI clockSet = new ClockReceiveInput();
		
		MicrowaveStateI tempRes = new IdleTime();
		
		//switch function to act according the keyword parsed.
		switch(keycode) {
			case '0': r1.storeNewResult("0");
					timeSec = timeSec + "0";
					tempRes = dispNo;
					break;
			case '1': r1.storeNewResult("1");
			timeSec = timeSec + "1";
					tempRes = dispNo;
					break;
			case '2':r1.storeNewResult("2");
			timeSec = timeSec + "2";
					tempRes = dispNo;
					break;
			case '3':r1.storeNewResult("3");
			timeSec = timeSec + "3";
					tempRes = dispNo;
					break;
			case '4':r1.storeNewResult("4");
			timeSec = timeSec + "4";
					tempRes = dispNo;
					break;
			case '5':r1.storeNewResult("5");
			timeSec = timeSec + "5";
					tempRes = dispNo;
					break;
			case '6':r1.storeNewResult("6");
			timeSec = timeSec + "6";
					tempRes = dispNo;
					break;
			case '7':r1.storeNewResult("7");
			timeSec = timeSec + "7";
					tempRes = dispNo;
					break;
			case '8':r1.storeNewResult("8");
			timeSec = timeSec + "8";
					tempRes = dispNo;
					break;
			case '9':r1.storeNewResult("9");
			timeSec = timeSec + "9";
					tempRes = dispNo;
					break;
				
			case 'a': 
				
				if(Integer.valueOf(timeSec.trim()) != 0){
					
					r1.storeNewResult("Started cooking for" + timeSec);
					
					tempRes = cooking;
				}
				break;
			case 'b': timeSec = "0";
					tempRes = idle;
				break;
			case 'c': timeSec = "0";
				tempRes = clockSet;	
				break;
			default: tempRes = dispNo;	
		}
		return tempRes;
	}
	

}
