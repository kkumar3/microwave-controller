package microwaveOven.driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import microwaveOven.service.MicrowaveContext;
import microwaveOven.util.Results;

public class Driver {
	
	public static void main(String[] args){
		
		//checking input arguments
		if(args.length != 2){
			System.out.println("Invalid number of arguments provided. Expected is one input file and one output file.");
			System.exit(1);
		}
		
		MicrowaveContext m1 = new MicrowaveContext();
		
		
			Results r1;
			r1 = new Results(args[1]);
				
			
			String instruction;
		
		
		//read input
				
			try{
				
				File f = new File(args[0]);
			
				BufferedReader b = new BufferedReader(new FileReader(f));
				
				
				while ((instruction = b.readLine())!= null) {
					
						m1.action(instruction.charAt(0), r1); // calling action of Microwaveoven context
				}											  // and passing an instance of Result class to store results	
				
			} catch (IOException e) {
				System.err.println("There is no input file present");
				e.printStackTrace();
				System.exit(1);
			} 
			
			Results.writer.close();	
		
		}
		
		
	}


